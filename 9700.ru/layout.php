<?php
/**
 * Footer layout
 *
 * @package OceanWP WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<footer id="footer" class="<?php echo esc_attr( oceanwp_footer_classes() ); ?>"<?php oceanwp_schema_markup( 'footer' ); ?>>

    <?php do_action( 'ocean_before_footer_inner' ); ?>

    <div id="footer-inner" class="clr">

<div class="footer-sets">
  <div class="footer-container">
    <div class="footer-col ">
      <h5 class="footer-header">Сервера и Сети</h5>
      <a href="http://2018.9700.ru/server-side-os-installation/"  class="footer-list-item">Установка серверных ОС</a>
      <a href="http://2018.9700.ru/mikrotik-and-ros/"  class="footer-list-item">MikroTik и RoS</a>
      <a href="http://2018.9700.ru/virtualization/"  class="footer-list-item">Виртуализация</a>
      <a href="http://2018.9700.ru/monitoring-and-backup/"  class="footer-list-item">Мониторинг и резервирование</a>
      <a href="http://2018.9700.ru/cloud-technologies/"  class="footer-list-item">"Облачные" технологии</a>
    </div>  
    <div class="footer-col ">
      <h5 class="footer-header">Физ и Юр лицам</h5>
      <a href="http://2018.9700.ru/consulting-and-training/"  class="footer-list-item">Консультации и обучение</a>
      <a href="http://2018.9700.ru/crm-and-erp-systems-integration/"  class="footer-list-item">CRM / ERP системы, интеграция</a>
      <a href="http://2018.9700.ru/1c-accounting-and-business-processes/"  class="footer-list-item">1C, учет, бизнес-процессы</a>
      <a href="http://2018.9700.ru/printing-management/"  class="footer-list-item">Управление печатью</a>
      <a href="http://2018.9700.ru/reducing-cost-of-communication-and-roaming/"  class="footer-list-item">Уменьшение затрат на связь, роуминг</a>
      <a href="http://2018.9700.ru/urgent-internet/"  class="footer-list-item">"Срочный" интернет</a>
      <a href="http://2018.9700.ru/equipment-and-software-selection/"  class="footer-list-item">Подбор оборудования и ПО</a>
    </div>
    <div class="footer-col ">
      <h5 class="footer-header">WEB</h5>
      <a href="http://2018.9700.ru/servers-applications-websites/"  class="footer-list-item">Сервера, приложения, сайты</a>
      <a href="http://2018.9700.ru/website-support/"  class="footer-list-item">Помощь с сайтом</a>
      <a href="http://2018.9700.ru/audit-and-specification/"  class="footer-list-item">Аудит, тех. задания</a>
      <a href="http://2018.9700.ru/online-broadcasts-and-video-portals/"  class="footer-list-item">Online трансляции, видеопорталы</a>
    </div>
    <div class="footer-col ">
      <h5 class="footer-header">Связь</h5>
      <a href="http://2018.9700.ru/voip-servers-and-clients/"  class="footer-list-item">VoIP сервера и клиенты</a>
      <a href="http://2018.9700.ru/billing-auto-calling-transport/"  class="footer-list-item">Биллинг, авто обзвон, транспорт</a>
      <a href="http://2018.9700.ru/wi-fi-networks-and-roaming/"  class="footer-list-item">Wi-Fi сети и роуминг</a>
      <a href="http://2018.9700.ru/vpn-consolidation-of-offices/"  class="footer-list-item">VPN, объединение офисов</a>
    </div>
    <div class="copyrights-container">
      <p class="copyrights">© 2017 ЭргономикС. All Rights Reserved.</p>
    </div>
  </div>
</div>

		
        <?php
        // Display the footer widgets if enabled
        //if ( oceanwp_display_footer_widgets() ) {
        //	get_template_part( 'partials/footer/widgets' );
        //}

        // Display the footer bottom if enabled
        //if ( oceanwp_display_footer_bottom() ) {
        //	get_template_part( 'partials/footer/copyright' );
        //} ?>
        
    </div><!-- #footer-inner -->

    <?php do_action( 'ocean_after_footer_inner' ); ?>

</footer><!-- #footer -->