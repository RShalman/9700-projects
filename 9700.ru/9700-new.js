var $ = jQuery;

$( document ).ready(function() {

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$( window ).on( "load", function() {
        $(".anim").removeClass("notactive");
        $(".anim").addClass("active");
        $(".header-bg").addClass("header-bg-show");
    });

$(window).on('resize scroll', function() {
  $('.anim').each(function() {
    
    if ($(this).isInViewport()) {
            $(".anim").removeClass("notactive");
            $(".anim").addClass("active");
        }
        else {
            $(".anim").removeClass("active");
            $(".anim").addClass("notactive");
        }
    });
  });
});