
// var $ = jQuery;

jQuery(document).ready(function() {

// Adds sidebar ability to slide in and out
    $('.leftmenu').on('click', function() {
        $('.menucontainer').toggleClass('open');
        $('.leftmenu__shop').toggleClass('active');
        $('.leftmenu__closemenu').toggleClass('active');
        $('body').toggleClass('noScrollBar');
    });

// Adds timeout for link to appear on main categories
function timeOutForLink (){
	$(".menucontainer__menuitemtitle.chosen a.catalog-link").addClass("further-link");
   }

// Show submenus on hover for sidebar menu-items
    // $('menucontainer', function () {
			$('.menucontainer__menuitemtitle a.catalog-link').on('mouseover click touchstart',function () {
				$(".menucontainer__submenu, .bestseller-slider-container, .leftmenu-labaratory-banner").addClass('collapse');
				$(this).parent().closest('.menucontainer__menuitem').find(".menucontainer__submenu, .bestseller-slider-container, .leftmenu-labaratory-banner").removeClass('collapse');
				$(".menucontainer__menuitemtitle").removeClass('chosen');
				$(".menucontainer__menuitemtitle a.catalog-link").removeClass('further-link');
				$(this).parent().addClass('chosen');
				   setTimeout (timeOutForLink,2000);
			});
			$('.menucontainer__menuitemtitle a.catalog-link').on('mouseover click touchstart',function (event) {
			  if ($(this).parent().siblings(".menucontainer__submenu, .bestseller-slider-container, .leftmenu-labaratory-banner").hasClass('collapse')) {
				  event.preventDefault();
				  $(this).parent().siblings(".menucontainer__submenu, .bestseller-slider-container, .leftmenu-labaratory-banner").removeClass('collapse');
			  }
			});
		//   });


// Availiability checker list
$('cart-availiability-container', function () {
    $('.cart-availiability-check-header, .check-city').mouseover(function () {
        $(".check-city-pos-container").addClass('collapse');
        $(this).parent().siblings('.cart-check-cities').removeClass('collapse');
    });
    
    $('.check-city').mouseover(function (event) {
        if ($(this).siblings(".check-city-pos-container").hasClass('collapse')) {
            event.preventDefault();
            $(this).nextAll(".check-city-pos-container").first().removeClass('collapse');
        }  
    });
    $('.cart-footer').mouseleave(function () {
        $(".check-city-pos-container").addClass('collapse');
        $(".cart-check-cities").addClass('collapse');
    });
  });

// Catalog page filter dropdown
$('catalog-filter-menu', function () {
    $('.catalog-filter-menu-title').mouseover(function () {
        $(".catalog-filter-menu-dropdown").addClass('collapse');
        $(".catalog-filter-menu-title").removeClass('active-filter');
        $(this).siblings('.catalog-filter-menu-dropdown').removeClass('collapse');
        $(this).addClass('active-filter');
    });
    
    $('.catalog-filter-menu-title').mouseover(function (event) {
        if ($(this).siblings(".catalog-filter-menu-dropdown").hasClass('collapse')) {
            event.preventDefault();
            $(this).nextAll(".catalog-filter-menu-dropdown").first().removeClass('collapse');
        }  
    });

    // Adds ability to close dropdown on click outsidde container
    $(document).mouseup(function(e) {
        var container = $("#cat-filter-menu");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            $(".catalog-filter-menu-dropdown").addClass('collapse');
            $(".catalog-filter-menu-title").removeClass('active-filter');
        }
    });
      
  });

  // Product card page dropdowns
  $('product-card-dropdown-container', function () {
    $('.product-card-dropdown-button').mouseover(function () {
        $(".product-card-dropdown-items").addClass('collapse');
        $(this).parent().siblings('.product-card-dropdown-items').removeClass('collapse');
    });
    
    $('.product-card-dropdown-container, .product-card-dropdown-items').mouseleave(function () {
        $(".product-card-dropdown-items").addClass('collapse');
    });
  });

  if (window.matchMedia("(max-width: 767px)").matches) {
	$('product-card-dropdown-container', function () {
		$(this).click(function () {
			$('#active_stores .product-card-dropdown-items').toggleClass('collapse');
		});
	});
  }

// // Add Shop-page link on sidebar
// $('<a href="#">Все товары</a>').prependTo('.sub-menucontainer');

// // Cart on slide (one-page-cart)				

// $(window).load(function () {
//     $('.woofc').prependTo('.container-for-cart');
//     $('.container-for-cart .woofc-inner').appendTo('.container-for-cart aside');
//     $('header a[href*="/cart"]').addClass('woofc-trigger woofc-icontype-image');
//     $('ul.woofc-count').prependTo('header a[href*="/cart"]');
//     $('a.woofc-trigger').attr('onclick', 'return false');
//     $('<h4 class="cart-title">Корзина</h4>').prependTo('.container-for-cart aside');
// });



// $('<div class="container-for-cart"></div>').prependTo('.fl-page');
// $('<div class="basketpage__close"></div>').prependTo('.container-for-cart');
// $('<aside class="fl-widget woocommerce widget_shopping_cart"></aside>').prependTo('.container-for-cart');
// $('a[href*="/cart"]').attr('onclick', 'return false');

// Ability for cart to slide in/out
 $('li.cart, .basket-close, .cart-keep-shopping').click(function () {
     $('.container-for-cart').toggleClass('openned-list');
     $("html").toggleClass('hide-overflow');	
 });

// Prevent scrolling inside "Cart on slide" div --ADDED + LEFTMENU CONTAINER
 $('.container-for-cart, .menucontainer').on( 'mousewheel DOMMouseScroll', function (e) { 
     var e0 = e.originalEvent;
     var delta = e0.wheelDelta || -e0.detail;
  
     this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
     e.preventDefault();  
   });
   
// Cart modal for POS checker

    $modal = $('.modal-frame');
    $overlay = $('.modal-overlay');

    /* Need this to clear out the keyframe classes so they dont clash with each other between enеer/leave. */
    $modal.bind('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e){
        if($modal.hasClass('state-leave')) {
        $modal.removeClass('state-leave');
        }
    });

    $('.check-close').on('click', function(){
        $overlay.removeClass('state-show');
        $modal.removeClass('state-appear').addClass('state-leave');
    });

    $('.pos-check').on('click', function(){
        var id = $(this).attr('rel');
		var nm = $(this).html();
		var cnt = $('#ti').val();
		$('#mloc_name').html(nm);
		$('#mloc_addr').html($('#a-'+id).val());
		$('#mloc_phone').html($('#p-'+id).val());
		$('#mloc_phone').prop("href", "tel:"+$('#p-'+id).val());
		$('#mloc_work').html($('#w-'+id).val());
		var cid = 0;
		for(var i = 0; i < cnt; i++) {
			cid = $('#i-'+i).val();
			$('#iq-'+cid).html($('#q_'+id+'_'+cid).val());
		}
        $overlay.addClass('state-show');
        $modal.removeClass('state-leave').addClass('state-appear');
    });

    $('.cart-item-minus').click(function () {
		var bid = $(this).parent().parent().attr('id');
		var qty = parseInt($('#bqty-'+bid).html())-1;
		if(qty>0) {
			var pr = parseInt($('#bpr-'+bid).val());
			var sum = parseInt($('#bsum-'+bid).html())-pr;
			var tsum = parseInt($('#btsum').html())-pr;
			$('#bqty-'+bid).html(qty);
			$('#bsum-'+bid).html(sum);
			$('#btsum').html(tsum);
		}
		SetQtyBasket(bid,qty);
	});

	$('.cart-item-plus').click(function () {
		var bid = $(this).parent().parent().attr('id');
		var pr = parseInt($('#bpr-'+bid).val());
		var qty = parseInt($('#bqty-'+bid).html())+1;
		var sum = parseInt($('#bsum-'+bid).html())+pr;
		var tsum = parseInt($('#btsum').html())+pr;
		$('#bqty-'+bid).html(qty);
		$('#bsum-'+bid).html(sum);
		$('#btsum').html(tsum);
		SetQtyBasket(bid,qty);
	});

	$('.cart-item-del-btn').click(function () {
		var bid = $(this).parent().parent().parent().attr('id');
		var cnt = parseInt($('#ti').val())-1;
		$('#'+bid).hide();
		$('#m-'+bid).hide();
		$('#ti').val(cnt);
		var sum = parseInt($('#bsum-'+bid).html());
		var tsum = parseInt($('#btsum').html())-sum;
		$('#btsum').html(tsum);
		if(cnt==0){
			$('.cart-desc-and-items-container').html('<p class="basket-empty">Здесь появятся товары после добавления в корзину. <a href="/catalog/">Открыть каталог</a></p></div>');
			$('.cart-footer').hide();
			$('#top_card').html('');
		} else $('#top_card').html(cnt);
		SetQtyBasket(bid,0);
	});
	
	function SetQtyBasket(bid,qty) {
		var request; 
		if(window.XMLHttpRequest){ 
			request = new XMLHttpRequest(); 
		} else if(window.ActiveXObject){ 
			request = new ActiveXObject("Microsoft.XMLHTTP");  
		} else { 
			return; 
		} 
		request.open('POST', '/script/ct_chqty_basket.php', true);
		request.onreadystatechange = function(){
		  switch (request.readyState) {
					  //case 1: print_console("<br/><em>1: Подготовка к отправке...</em>"); break
					  //case 2: print_console("<br/><em>2: Отправлен...</em>"); break
					  //case 3: print_console("<br/><em>3: Идет обмен..</em>"); break
			case 4:{
				if(request.status==200){//print_console("<br/><em>4: Обмен завершен.</em>"); 
						//alert("<br/><em>4: Обмен завершен.</em>");
						  //document.getElementById('rate'+pid).innerHTML = request.responseText;
					//alert(request.responseText);						  
				} else if(request.status==404){
						alert("Ошибка: запрашиваемый скрипт не найден!");
				} else alert("Ошибка: сервер вернул статус: "+ request.status);
				break
			}
		  }		
		} 
		var params = 'bid='+bid+'&qty='+qty;
		//alert(params);
		request.setRequestHeader("Content-Type","application/x-www-form-urlencoded"); 
		request.send(params);
	}
	
	$('.cart-goto-checkout').click(function () {
		window.location.href = "/order/";
	});

// Adds username to "sign-in" button when logged
    // var getUsrName = $('div.get_user_name').text();
    // $('body.logged-in ul.nav a[href*="/my-account/"]').text(getUsrName);
    // $('div.get_user_name').remove();

    $('.filter-by-country').click(function () {
		window.location.href = "/brands/?c="+$(this).attr('rel');
	});

	$('#brand_sort').click(function () {
		var a = "A-Z";
		var z = "Z-A";
		var c = $('#brand_country').val();
		var u = '/brands/?r=';
		var t = $('#brand_sort_title').html();
		if(t==z) { u = u+'a'; }
		else { u = u+'z'; }
		if(c>0) u = u+'&c='+c;
		window.location.href = u;
		//alert($('#brand_sort_title').html());
	}); 

	$('.filter-by-category').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?section="+$(this).attr('rel');
	});

	$('.filter-by-brand').click(function () {
		var section = "";
		var sid = $('#PARAM_SECTION').val();
		if(sid > 0) section = '&section='+sid;
		window.location.href = "/catalog/?brand="+$(this).attr('rel')+section;
	});
	
	$('.filter-by-type').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?type="+$(this).attr('rel');
	});
	
	$('.filter-by-action').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?action="+$(this).attr('rel');
	});
	
	$('.filter-by-skin').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?skin="+$(this).attr('rel');
	});
	
	$('.filter-by-ingr').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?ingr="+$(this).attr('rel');
	});
	
	$('.filter-by-color').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?color="+$(this).attr('rel');
	});
	
	$('.filter-by-density').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?density="+$(this).attr('rel');
	});
	
	$('.filter-by-finish').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?finish="+$(this).attr('rel');
	});
	
	$('.filter-by-sex').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?sex="+$(this).attr('rel');
	});
	
	$('.filter-by-price').click(function () {
		//alert($(this).attr('rel'));
		window.location.href = "/catalog/?price="+$(this).attr('rel');
	});

	
	$('.ordlist_sort').change(function () {
		var s = $(this).parent().attr('rel')+$(this).attr('rel');
		window.location.href = "/profile/?s="+s;
	});

	$('.product-card-add-to-card-btn').click(function () {
		$('form.add-to-basket-detail').submit();
	});
	  
	var addToCart = function(){
		if (!this) return;
		var d = $(this).serialize();
		$.get('/script/ct_add_to_cart.php',
			d,
			$.proxy(
				function(data) {
					//$.get('/script/ct_get_cart.php', function(data) {
					//	$('.container-for-cart').html(data);
					//});
					$('#basket_result').html(data);
					var tc = $('#top_card').html();
					if(tc=='') { tc = 1; } else tc = parseInt(tc)+1;
					$('#top_card').html(tc);
					setTimeout(function() { location.reload(); },2000);
					return;
				}
			)
		);
		return false;
	}

	$(function(){
		$('form.add-to-basket').submit(addToCart);
	});

	$(function(){
		$('form.add-to-basket-detail').submit(addToCart);
	});
	
	$('#show_iiko_log').click(function () {
		$('#iiko_log').hide();
		var oid = parseInt($('#order_for_log').val());
		if(oid > 0) {
			$.ajax({
			url: "/script/get_iiko_log.php",
			type: 'POST',
			data: {'oid': oid},
			success: function(html){
				$('#iiko_log').html(html);
				$('#iiko_log').show();
			}
		});
		} else alert("Укажите корректный номер заказа");
		return false;
	});
	
	$(function() {
		$("#main_search").autocomplete({
			source: '/script/suggest.php',
			select: function(event, ui) { 
				$("#main_search").val(ui.item.label);
				$("#search_form").submit(); 
			}
		});
	})
// Account page change payment method dropdown
    $('.account-page-change-payment-method-btn').on('click', function(){
        $('.account-page-change-payment-method-subcontainer').toggleClass('change-method-opener')
    });

// Account page change payment method (cash/card)
    $('.payment-method-choice').on('click', function(){
        $(this).addClass('payment-method-selected');
        $(this).siblings().removeClass('payment-method-selected');
    });

// Account page order history items block opener (by clicking on triangle)
    $('.account-page-order-composition-opener').on('click', function(){
        $(this).parent().siblings('.account-page-order-composition-history-items-container').toggle(1000);
        $(this).toggleClass('arrow-opener');
	});
	
// Depricated due to PHP solution
// Add spaces between prices (formatting prices): WAS - 123456789 / BECOME - 123 456 789
// var takenPriceById = parseInt($('[id*="_price"]').text(), 10);
// var priceWithSpacesById = takenPriceById.toLocaleString();
// $('[id*="_price"]').html(priceWithSpacesById);
//    $('[class="catalog-element-inner-price-container"]').each(function () {

// 		 $(this).text(parseInt($(this).text(), 10).toLocaleString());
// 	});

// Adds classes to inputs on login page so that it comes red when empty on submit click (works separatly for Log/Reg)
	$('#existing-customer input[type="submit"]').on('click', function() {
		// Adds or removes class 'onErrorMsg' if input of phone or pswd is empty or not
		$('#existing-customer input:not([type="submit"])').each(function(){
			if( $(this).val().length === 0 ) {
				$(this).addClass('onMsgError');
				$('#existing-customer .msg').addClass('test');
			} else if ( $(this).val().length > 0 ) {
				$(this).removeClass('onMsgError');
				$('#existing-customer .msg').removeClass('test');
			}
		})
		// Removes class if another submit (from another field) is clicked
		$('#new-customer input:not([type="submit"])').each(function(){
			$(this).removeClass('onMsgError');
		})

		$('#new-customer .msg').each(function () {
			$(this).removeClass('errorMessage');
		});
	});
	
	$('#new-customer input[type="submit"]').on('click', function() {
		// Adds or removes class 'onErrorMsg' if input of phone or pswd is empty or not
		$('#new-customer input:not([type="submit"])').each(function(){
			if( $(this).val().length === 0 ) {
				$(this).addClass('onMsgError');
				$('#new-customer .msg').addClass('errorMessage');
			} else if ( $(this).val().length > 0 ) {
				$(this).removeClass('onMsgError');
				$('#new-customer .msg').removeClass('errorMessage');
			}
		})
		// Removes class if another submit (from another field) is clicked
		$('#existing-customer input:not([type="submit"])').each(function(){
			$(this).removeClass('onMsgError');
		})

		$('#existing-customer .msg').each(function () {
			$(this).removeClass('test');
		});
	});

// Leftmenu behaviour for tablet + phone (dropdown onclick + link working if submenu is open)
	if (window.matchMedia("(max-width: 1199px)").matches) { 
		// $('.catalog-link').on('click touchstart', function () {
		// 	if($(this).hasClass('further-link')) {
		// 		return true;
		// 	} else {
		// 		return false;
		// 	}
		// });

		// Closes menuitemtitle on SPAN touch
		// $('.menu-left-arrow').on('touchstart', function() {
		// 	$(this).parent().removeClass('chosen');
		// });

		// Mobile search open/close btn
		// $('.mobile-search-btn').on('click', function() {
		// 	$('.search').toggleClass('openned-search')
		// });


	  }
	  

	//    $('.mobile-menu-btn').on('click', function(){
	//  	alert('test');
	//  	$('.mobile-menu-btn').toggleClass('openned-menu');
	//    });
	  
// Mobile main menu
	// document.querySelector('.mobile-menu-btn').setAttribute("ontouchstart", "openMobMenu()");

	// function openMobMenu() {
	// 	alert('test');
	// 		$('.mobile-menu-btn').toggleClass('openned-menu');
	// }


// ON DOCUMENT READY ENDS HERE!!!
});