<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<?//echo '<pre>'; print_r($arResult); echo '</pre>';?>
<?
$arID = array();

$arBasketItems = array();

$dbBasketItems = CSaleBasket::GetList(
     array(
                "NAME" => "ASC",
                "ID" => "ASC"
             ),
     array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
             ),
     false,
     false,
     array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
             );
while ($arItems = $dbBasketItems->Fetch())
{
     if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
     {
          CSaleBasket::UpdatePrice($arItems["ID"],
                                 $arItems["CALLBACK_FUNC"],
                                 $arItems["MODULE"],
                                 $arItems["PRODUCT_ID"],
                                 $arItems["QUANTITY"],
                                 "N",
                                 $arItems["PRODUCT_PROVIDER_CLASS"]
                                 );
          $arID[] = $arItems["ID"];
     }
}
if (!empty($arID))
     {
     $dbBasketItems = CSaleBasket::GetList(
     array(
          "NAME" => "ASC",
          "ID" => "ASC"
          ),
     array(
          "ID" => $arID,
        "ORDER_ID" => "NULL"
          ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
                );
while ($arItems = $dbBasketItems->Fetch())
{
    $arBasketItems[] = $arItems;
}
}
$res = CIBlockElement::GetByID($arItems["PRODUCT_ID"]);
// Печатаем массив, содержащий актуальную на текущий момент корзину
echo "<pre>";
print_r($arBasketItems);
echo "</pre>";
?>
	<div class="inner-cart-container">
        <h1 class="cart-header"><?=GetMessage("CT_BASKET_TITLE");?></h1>
        <h5 class="terms"><a href="/delivery/"><?=GetMessage("CT_BASKET_COND");?></a></h5>
	<?if(empty($arBasketItems)) { echo GetMessage("CT_BASKET_NO_ITEMS"); }
	else {?>
		<div class="cart-desc-and-items-container">
			<div class="cart-description-container">
                <h5 class="cart-desc-element cart-product"><?=GetMessage("CT_BASKET_PROD");?></h5>
                <h5 class="cart-desc-element cart-price"><?=GetMessage("CT_BASKET_PRICE");?></h5>
                <h5 class="cart-desc-element cart-amount"><?=GetMessage("CT_BASKET_QTY");?></h5>
                <h5 class="cart-desc-element cart-total"><?=GetMessage("CT_BASKET_TOTAL");?></h5>
            </div>
			<?foreach($arBasketItems as $item) {?>
			<div class="cart-item-container" id="<?=$item["BID"];?>">
                <div class="cart-item-desc-and-brand">
                    <?$getImage = CFile::GetPath($arItem["PREVIEW_PICTURE"]);?>
                    <img src="<?=$getImage["src"];?>" alt="<?=$item["NAME"];?>" class="cart-item-img">
                    <div class="cart-item-desc-container">
                        <h5 class="cart-item-brand"><?=$item["BNAME"];?></h5>
                        <h5 class="cart-item-text"><?=$item["NAME"];?></h5>
                    </div>
                </div>
                <h4 class="cart-item-price"><?=$item["PRICE"];?> &#x20bd;</h4>
				<input type="hidden" id="bpr-<?=$item["BID"];?>" value="<?=$item["PRICE"];?>" />
                <div class="cart-item-quantity-container">
                    <a href="javascript:void(0);" class="cart-item-minus">-</a>
                    <h1 class="cart-item-quantity" id="bqty-<?=$item["BID"];?>"><?=$item["QTY"];?></h1>
                    <a href="javascript:void(0);" class="cart-item-plus">+</a>
                </div>
                <div class="cart-item-subtotal-and-del-container">
                    <div class="cart-item-subtotal-price-container">
                        <h4 class="cart-item-subtotal-price"><span id="bsum-<?=$item["BID"];?>"><?=$item["SUM"];?></span> &#x20bd;</h4>
                    </div>
                    <div class="cart-item-del-btn-container">
                        <h2 class="cart-item-del-btn">x</h2>
                    </div>
                </div>
            </div>
			<?}?>
			<div class="cart-item-total-container">
                <h1 class="cart-item-total"><span id="btsum"><?=$arResult["SUM"];?></span> &#x20bd;</h1>
            </div>
		</div>	
		<div class="cart-footer">
            <div class="cart-availiability-container">
                <div class="cart-availiability-check">
                    <h5 class="cart-availiability-check-header"><?=GetMessage("CT_BASKET_VIEW");?></h5>
                </div>
                <div class="cart-check-cities collapse">
				<?foreach($arResult["LOC"] as $s=>$locs) {?>
                    <h5 class="check-city"><?=$s;?></h5>
                    <div class="check-city-pos-container collapse">
					<?foreach($locs as $li=>$loc) {?>
						<h5 class="pos-check" rel="<?=$li;?>"><?=$loc["NAME"];?></h5>
						<input type="hidden" id="a-<?=$li;?>" value="<?=$loc["ADDRESS"];?>" />
						<input type="hidden" id="p-<?=$li;?>" value="<?=$loc["PHONE"];?>" />
						<input type="hidden" id="w-<?=$li;?>" value="<?=$loc["WORK"];?>" />
					<?}?>
                    </div>
				<?}?>			
				</div>
            </div>
            <div class="cart-buttons-container">
                <a role="button" tabindex="0" class="cart-keep-shopping"><?=GetMessage("CT_BASKET_PROCEED");?></a>
                <a href="javascript:void(0);" class="cart-goto-checkout"><?=GetMessage("CT_BASKET_ORDER");?></a>
            </div>
        </div>
	</div>
	<div class="basket-close">X</div>
	<div class="modal-frame">
	<?$msg = array(GetMessage("CT_BASKET_Q0"),GetMessage("CT_BASKET_Q1"),GetMessage("CT_BASKET_Q2"));
	foreach($arResult["BALANCE"] as $t=>$tv) {
		foreach($tv as $p=>$pv) {?>
		<input type="hidden" id="q_<?=$t;?>_<?=$p;?>" value="<?=$msg[$pv];?>" />
		<?}
	}?>
        <input type="hidden" id="ti" value="<?=$arResult["CNT"];?>" />
		<div class="modal">
            <div class="modal-inset">
                <div class="button check-close">
                    <h5 class="pos-check-modal-close">x</h5>
                </div>
                <div class="modal-body">
					<div class="modal-pos-check-container">
                        <div class="pos-info-container">
                            <h1 class="pos-check-modal-header" id="mloc_name"></h1>
                            <h5 class="pos-check-modal-address" id="mloc_addr"></h5>
                            <a href="" class="pos-check-modal-phone" id="mloc_phone"></a>
                            <h5 class="pos-check-modal-time" id="mloc_work"></h5>
                        </div>
                        <div class="pos-items-container">
                        <?$i = 0;
						foreach($arResult["ITEMS"] as $item) {?>    
							<div class="pos-check-modal-item-container" id="m-<?=$item["BID"];?>">
                                <input type="hidden" id="i-<?=$i;?>" value="<?=$item["ID"];?>" />
								<div class="pos-check-modal-img-and-desc-container">
                                    <img src="<?=$item["PICTURE"];?>" alt="" class="pos-check-modal-item-img">
                                    <div class="pos-check-modal-brand-and-desc">
                                        <h5 class="pos-check-modal-brand"><?=$item["BNAME"];?></h5>
                                        <h5 class="pos-check-modal-desc"><?=$item["NAME"];?></h5>
                                    </div>
									<h4 class="pos-check-modal-price"><?=$item["PRICE"];?> &#x20bd;</h4>
                                    <h3 class="pos-check-modal-quantity" id="iq-<?=$item["ID"];?>"></h3>
                                </div>
                            </div>
						<?$i++;
						}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?}?>	