<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false); 
?>
<?//echo '<pre>'; print_r($arResult); echo '</pre>';
$icnt = $slides = array();
foreach($arResult["ITEMS"] as $id=>$item) {
    $icnt[] = $id;
	$slides[$id] = $sections[$id] = '';
	foreach($item["SLIDER"] as $slide) $slides[$id] .= '<li class="glide__slide"><img src="'.$slide.'" alt="" srcset=""></li>';
}
$slider = '<div class="product-card-img-slider-container">
                <div id="options-per-view" class="glide">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides" id="active_slider">'.$slides[$icnt[0]].'</ul>
                    </div>
                    <div class="glide__arrows" data-glide-el="controls">
                        <a class="glide__arrow glide__arrow--left" data-glide-dir="<">&#8249;</a>
                        <a class="glide__arrow glide__arrow--right" data-glide-dir=">">&#8250;</a>
                    </div>
                </div>
                <script>
                    new Glide(".glide", {
                        type: "slider",
                        startAt: 0,
                        perView: 1
                    }).mount()
                </script>
            </div>';

$arRs = Array("slides"=>$slides,"sections"=>$sections,"sku"=>$sku,"volumes"=>$volumes,"shades"=>$shades,"prices"=>$prices,"priceids"=>$priceids,"notes"=>$notes,"cares"=>$cares,"usages"=>$usages,"ingrs"=>$ingrs);		
?>
<script>
	var arRs=<?=CUtil::PhpToJSObject($arRs); ?>;
	function setActiveProduct(id) {
		$('#frm_id').val(id);
		if(!!arRs['slides'][id]) { 
			$('#active_slider').html(arRs['slides'][id]);
			new Glide(".glide", {
                    type: "slider",
					startAt: 0,
                    perView: 1
                }).mount();
		}			
	}
</script>
<?/**/?>
