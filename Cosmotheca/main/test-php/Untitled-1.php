<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Context,
	Bitrix\Main\Type\DateTime,
	Bitrix\Main\Loader,
	Bitrix\Iblock;

$arParams["SEF_DIR"] = trim($arParams["SEF_DIR"]);
if(strlen($arParams["SEF_DIR"])<=0)
	$arParams["SEF_DIR"] = "/products/";

$arParams["SEF_TMPL"] = trim($arParams["SEF_TMPL"]);
if(strlen($arParams["SEF_TMPL"])<=0)
	$arParams["SEF_TMPL"] = "#BRAND_CODE#/#CODE#/";

$arVariables = array();
CComponentEngine::ParseComponentPath(
   $arParams["SEF_DIR"],
   array($arParams["SEF_TMPL"]),
   $arVariables
);

$arResult["CODE"] = $arVariables["CODE"];

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
	$arParams["IBLOCK_TYPE"] = "catalog";

if(!Loader::includeModule("iblock"))
{
	$this->abortResultCache();
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}
$brandFound = false;
if(strlen($arVariables["BRAND_CODE"])>0) {
$rsBrand = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>4,"CODE"=>$arVariables["BRAND_CODE"]), false, false, array("ID","NAME","DETAIL_TEXT","PREVIEW_PICTURE"));
	if($obBrand = $rsBrand->GetNext()) {
		$brandFound = true;
		$arFilter["PROPERTY_BRAND"] = $obBrand["ID"];
		$arResult["BRAND"]["ID"] = $obBrand["ID"];
		$arResult["BRAND"]["CODE"] = $arVariables["BRAND_CODE"];
		$arResult["BRAND"]["NAME"] = $obBrand["NAME"];
		$arResult["BRAND"]["TEXT"] = $obBrand["DETAIL_TEXT"];
		$arFile = CFile::GetFileArray($obBrand["PREVIEW_PICTURE"]);
		if($arFile) $arResult["BRAND"]["PICTURE"] = $arFile["SRC"];
	}
} 	

	if(intval($arParams["IBLOCK_ID"]) > 0)
		$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
	$arFilter["CODE"] = $arVariables["CODE"];
	$arSelect = array(
		"ID",
		"NAME",
		"XML_ID",
		"IBLOCK_ID",
	//by Shalman	//"PREVIEW_TEXT",
	//by Shalman	//"PREVIEW_PICTURE",
		"DETAIL_TEXT",
		"DETAIL_PICTURE",
		"CATALOG_GROUP_1",
	);
	$rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if(intval($rsElement->SelectedRowsCount())>0 && $brandFound)
	{
	  global $DB;
	  $arDelTerm = array();
	  $rsD = $DB->Query("SELECT ID,NAME FROM siiko_terminals");
	  while($arD = $rsD->GetNext()) $arDelTerm[$arD["ID"]] = $arD["NAME"];
	  $cardType = 0;
	  while	($obElement = $rsElement->GetNextElement()) {
		$arRes = $obElement->GetFields();
		$arProps = $obElement->GetProperties();
		$arRes["PROPS"] = $slider = array();
		foreach($arProps as $pid=>$prop)
		{
		  if((is_array($prop["VALUE"]) && count($prop["VALUE"])>0) || (!is_array($prop["VALUE"]) && strlen($prop["VALUE"])>0)){
			$arRes["PROPS"][$pid] = array("ID"=>$prop["ID"],"NAME"=>$prop["NAME"],"PROPERTY_TYPE"=>$prop["PROPERTY_TYPE"],"VALUE"=>$prop["VALUE"]);
			if($pid != "PICTURES") {
			  if(is_array($prop["VALUE"]) && count($prop["VALUE"])>0) {	
			    foreach($prop["VALUE"] as $pk=>$pv) {
				  $arRes["PROPS"][$pid]["TXT"][$pk] = $pv;
				  if($prop["PROPERTY_TYPE"]=="E") { 
					$rp = CIBlockElement::GetByID($pv);
					if($ar_rp = $rp->GetNext()) {$arRes["PROPS"][$pid]["TXT"][$pk] = $ar_rp["NAME"];}
				  }  
			    }
			  } else {
				$arRes["PROPS"][$pid]["TXT"] = $prop["VALUE"];
				if($prop["PROPERTY_TYPE"]=="E") {
				  $rp = CIBlockElement::GetByID($prop["VALUE"]);
				  if($ar_rp = $rp->GetNext()) {$arRes["PROPS"][$pid]["TXT"] = $ar_rp["NAME"];}
				}  
			  }
			}
			if($pid == "SHADE") {
				$dbp = CIBlockElement::GetProperty(27, $prop["VALUE"], array("sort" => "asc"), Array("CODE"=>"RGB"));
				if($arp = $dbp->Fetch()) $arRes["PROPS"]["SHADE"]["RGB"] = $arp["VALUE"];
			}			
		  }
		}
		$arS = $arStore = array();
		$db_groups = CIBlockElement::GetElementGroups($arRes["ID"], true);
		while($ar_group = $db_groups->Fetch()) $arS[$ar_group["ID"]] = array("NAME"=>$ar_group["NAME"],"CODE"=>$ar_group["CODE"]);
		$rsDs = $DB->Query("SELECT BALANCE,TID FROM siiko_stoplist WHERE PRODUCTID=".'"'.$arRes["XML_ID"].'"');
		$tqty = 0;
		while($arDs = $rsDs->GetNext()) if(intval($arDs["BALANCE"])>0){
			$arStore[$arDs["TID"]] = array("S"=>$arDelTerm[$arDs["TID"]],"B"=>$arDs["BALANCE"]);
			$tqty += intval($arDs["BALANCE"]);
	    }
		$pp = $dp = '/images/no-image.png'; 
		// if(intval($arRes["PREVIEW_PICTURE"])>0) {
		//   $arFile = CFile::GetFileArray($arRes["PREVIEW_PICTURE"]);
		//   if($arFile) { $pp = $arFile["SRC"]; $slider[] = $arFile["SRC"]; }
		// } // by Shalman
		if(intval($arRes["DETAIL_PICTURE"])>0) {
		  $arFile = CFile::GetFileArray($arRes["DETAIL_PICTURE"]);
		  if($arFile) { $dp = $arFile["SRC"]; $slider[] = $arFile["SRC"]; }
		}
		$pcnt = count($arProps["PICTURES"]["VALUE"]);
		if($pcnt>0) {
			foreach($arProps["PICTURES"]["VALUE"] as $pk=>$pv) {
				$arFile = CFile::GetFileArray($pv);
				if($arFile) { $arRes["PROPS"]["PICTURES"]["SRC"][$pk] = $arFile["SRC"]; $slider[] = $arFile["SRC"]; }
			}
		}
		$price = explode(".", $arRes["CATALOG_PRICE_1"]);
		$arResult["ITEMS"][$arRes["ID"]] = array(
		  "ID"=>$arRes["ID"],
		  "NAME"=>$arRes["NAME"],
		  "XML_ID"=>$arRes["XML_ID"],
		//by Shalman   "PREVIEW_TEXT"=>$arRes["PREVIEW_TEXT"],
		//by Shalman   "PREVIEW_PICTURE_SRC"=>$pp,
		  "DETAIL_TEXT"=>$arRes["DETAIL_TEXT"],
		  "DETAIL_PICTURE_SRC"=>$dp,
		  "PRICEID"=>$arRes["CATALOG_PRICE_ID_1"],
		  "PRICE"=>$price[0],
		  "QTY"=>$tqty,
		  "SLIDER"=>$slider,
		  "PROPS"=>$arRes["PROPS"],
		);
		$arResult["ITEMS"][$arRes["ID"]]["SECTIONS"] = $arS;
		$arResult["ITEMS"][$arRes["ID"]]["STORE"] = $arStore;
		$arResult["STORES"] = $arDelTerm;
		if($cardType == 0) {
			if((is_array($arProps["T_NOTES"]["VALUE"]) && count($arProps["T_NOTES"]["VALUE"])>0) || (is_array($arProps["H_NOTES"]["VALUE"]) && count($arProps["H_NOTES"]["VALUE"])>0) || (is_array($arProps["B_NOTES"]["VALUE"]) && count($arProps["B_NOTES"]["VALUE"])>0)) {$cardType = 1;}
			else if((is_array($arProps["SKIN"]["VALUE"]) && count($arProps["SKIN"]["VALUE"])>0) || (is_array($arProps["ACTION"]["VALUE"]) && count($arProps["ACTION"]["VALUE"])>0) || (is_array($arProps["ACT_ARR"]["VALUE"]) && count($arProps["ACT_ARR"]["VALUE"])>0) || strlen($arProps["USE_RU"]["VALUE"])>0 || strlen($arProps["ACT_STR"]["VALUE"])>0) {$cardType = 2;}
			else $cardType = 3;
		}
		$cur_name = $arRes["NAME"];
	  }
	  $arResult["CARD"] = $cardType;
	  $APPLICATION->SetTitle($cur_name);

	  $this->includeComponentTemplate();
	}
	else
	{
		Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_DETAIL_NF")
			,true
			,true
			,true
			,$arParams["FILE_404"]
		);
	}