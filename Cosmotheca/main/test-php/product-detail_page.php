<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false); 
?>
<?//echo '<pre>'; print_r($arResult); echo '</pre>';
$brand_descr = '<div class="product-card-brand-description-container">
                    <div class="product-card-brand-desc-img-container">
                        <img src="'.$arResult["BRAND"]["PICTURE"].'" alt="'.$arResult["BRAND"]["NAME"].'" class="product-card-brand-logo">
                    </div>
                    <div class="product-card-brand-description">
                        <p>'.$arResult["BRAND"]["TEXT"].'</p>
                    </div>
                </div>';
$brand_name = '<div class="product-card-brand-container">
                    <h5 class="product-card-brand-name"><a href="/brand/'.$arResult["BRAND"]["CODE"].'/">'.$arResult["BRAND"]["NAME"].'</a></h5>
                </div>';
$icnt = $slides = $sections = $volumes = $shades = $sku = $prices = $priceids = $notes = $cares = $materials = $materials_seta_denticles = $usages = $ingrs = array();
$disp = '';
$cartbtns = '';
$storeblk = '';
$ci = 0;
foreach($arResult["ITEMS"] as $id=>$item) {
	$icnt[] = $id;
	$sku[$id] = $item["PROPS"]["SKU"]["NAME"].' '.$item["PROPS"]["SKU"]["VALUE"];
	$prices[$id] = $item["PRICE"];
	$priceids[$id] = $item["PRICEID"];
	if($ci>0) $disp = ' blk-add-to-card-btn-hidden';
	if(intval($item["QTY"])>0){

		if ($item["SHOP_QTY"] > 0) { 
			$cartbtns .= '<div id="blk-add-to-card-btn-'.$id.'" class="blk-add-to-card-btn'.$disp.'"><a href="javascript:void(0)" class="product-card-add-to-card-btn">'.GetMessage("CT_SF_ADD_TO_CART").'</a></div>'; 
		}
		
		$i = 0;
		$ddb = false;
		foreach($item["STORE"] as $store) {
			$stp = explode(":", $store["S"]);
			if($i==0) { 
				$storeblk .= '<div id="product-card-dropdown-store-container-'.$id.'" class="product-card-dropdown-store-container'.$disp.'"><h5 class="product-card-dropdown-button">'.$stp[0].' - '.$store["B"].' шт</h5></div>';
			} else {
				if(!$ddb) { $storeblk .= '<div class="product-card-dropdown-items collapse">'; $ddb = true;}
				$storeblk  .= '<h5 class="product-card-dropdown-item">'.$stp[0].' - '.$store["B"].' шт</h5>';
			}
			$i++;
		}
		if($ddb) $storeblk .= '</div>';
	} else {
		$cartbtns .= '<div id="blk-add-to-card-btn-'.$id.'" class="blk-add-to-card-btn'.$disp.'"><a href="javascript:void(0)" class="product-card-no-product-btn">'.GetMessage("CT_SF_INFORM").'</a><h5 class="product-card-no-product">'.GetMessage("CT_SF_NOT_AVAIL").'</h5></div>';
		$storeblk .= '<div id="product-card-dropdown-store-container-'.$id.'" class="product-card-dropdown-store-container'.$disp.'"><h5 class="product-card-dropdown-button">'.GetMessage("CT_SF_NOT_AVAIL").'</h5></div>';
	}
	$slides[$id] = $sections[$id] = '';
	foreach($item["SLIDER"] as $slide) $slides[$id] .= '<li class="glide__slide"><img src="'.$slide.'" alt="" srcset=""></li>';
	foreach($item["SECTIONS"] as $sid=>$section) $sections[$id] .= '<h5 class="product-card-category-name"><a href="/catalog/?section='.$sid.'">'.$section["NAME"].'</a></h5>';
	if(strlen($item["PROPS"]["VOLUME"]["VALUE"])>0) $volumes[$id] = $item["PROPS"]["VOLUME"]["VALUE"];
	if(strlen($item["PROPS"]["SHADE"]["VALUE"])>0) $shades[$id] = array("N"=>$item["PROPS"]["SHADE"]["TXT"],"C"=>$item["PROPS"]["SHADE"]["RGB"]);
	$notes[$id] = '';
	if(count($item["PROPS"]["T_NOTES"]["VALUE"])>0) {
		$notes[$id] .= '<div class="product-card-single-notes-list-container"><h5 class="product-card-single-notes-list-header">'.$item["PROPS"]["T_NOTES"]["NAME"].'</h5><ul class="product-card-notes-list">';
		foreach($item["PROPS"]["T_NOTES"]["TXT"] as $note) $notes[$id] .= '<li class="product-card-notes-item">'.$note.'</li>';
		$notes[$id] .= '</ul></div>';
	}
	if(count($item["PROPS"]["H_NOTES"]["VALUE"])>0) {
		$notes[$id] .= '<div class="product-card-single-notes-list-container"><h5 class="product-card-single-notes-list-header">'.$item["PROPS"]["H_NOTES"]["NAME"].'</h5><ul class="product-card-notes-list">';
		foreach($item["PROPS"]["H_NOTES"]["TXT"] as $note) $notes[$id] .= '<li class="product-card-notes-item">'.$note.'</li>';
		$notes[$id] .= '</ul></div>';
	}
	if(count($item["PROPS"]["B_NOTES"]["VALUE"])>0) {
		$notes[$id] .= '<div class="product-card-single-notes-list-container"><h5 class="product-card-single-notes-list-header">'.$item["PROPS"]["B_NOTES"]["NAME"].'</h5><ul class="product-card-notes-list">';
		foreach($item["PROPS"]["B_NOTES"]["TXT"] as $note) $notes[$id] .= '<li class="product-card-notes-item">'.$note.'</li>';
		$notes[$id] .= '</ul></div>';
	}
	$cares[$id] = '';
	if(count($item["PROPS"]["SKIN"]["VALUE"])>0) {
		$cares[$id] .= '<div class="product-card-single-care-list-container"><h5 class="product-card-single-care-list-header">'.$item["PROPS"]["SKIN"]["NAME"].'</h5><ul class="product-card-care-list">';
		foreach($item["PROPS"]["SKIN"]["TXT"] as $care) $cares[$id] .= '<li class="product-card-care-item">'.$care.'</li>';
		$cares[$id] .= '</ul></div>';
	}
	if(count($item["PROPS"]["ACTION"]["VALUE"])>0) {
		$cares[$id] .= '<div class="product-card-single-care-list-container"><h5 class="product-card-single-care-list-header">'.$item["PROPS"]["ACTION"]["NAME"].'</h5><ul class="product-card-care-list">';
		foreach($item["PROPS"]["ACTION"]["TXT"] as $care) $cares[$id] .= '<li class="product-card-care-item">'.$care.'</li>';
		$cares[$id] .= '</ul></div>';
	}
	if(count($item["PROPS"]["ACT_ARR"]["VALUE"])>0) {
		$cares[$id] .= '<div class="product-card-single-care-list-container"><h5 class="product-card-single-care-list-header">'.$item["PROPS"]["ACT_ARR"]["NAME"].'</h5><ul class="product-card-care-list">';
		foreach($item["PROPS"]["ACT_ARR"]["TXT"] as $care) $cares[$id] .= '<li class="product-card-care-item">'.$care.'</li>';
		$cares[$id] .= '</ul></div>';
    }
    $materials[$id] = '';
	if(count($item["PROPS"]["MATERIAL"]["VALUE"])>0) {
		$materials[$id] .= '<div class="product-card-single-material-container"><h5 class="product-card-single-material-header">'.$item["PROPS"]["MATERIAL"]["NAME"].'</h5><ul class="product-card-material-list">';
		foreach($item["PROPS"]["MATERIAL"]["TXT"] as $material) $materials[$id] .= '<li class="product-card-care-item">'.$material.'</li>';
		$materials[$id] .= '</ul></div>';
    }
    $materials_seta_denticles[$id] = '';
	if(count($item["PROPS"]["BRISTLE"]["VALUE"])>0) {
		$materials_seta_denticles[$id] .= '<div class="product-card-single-materials_seta_denticles-container"><h5 class="product-card-single-materials_seta_denticles-header">'.$item["PROPS"]["BRISTLE"]["NAME"].'</h5><ul class="product-card-materials_seta_denticles-list">';
		foreach($item["PROPS"]["BRISTLE"]["TXT"] as $material_seta_denticles) $materials_seta_denticles[$id] .= '<li class="product-card-care-item">'.$material_seta_denticles.'</li>';
		$materials_seta_denticles[$id] .= '</ul></div>';
	}
	$usages[$id] = '';
	if(strlen($item["PROPS"]["USE_RU"]["VALUE"])>0) $usages[$id] = '<h3 class="product-card-product-usage-description-header">'.$item["PROPS"]["USE_RU"]["NAME"].'</h3>
                    <div class="product-card-product-usage-description-text"><p>'.$item["PROPS"]["USE_RU"]["VALUE"].'</p></div>';
	$ingrs[$id] = '';
	if(strlen($item["PROPS"]["ACT_STR"]["VALUE"])>0) {
		$ingrs[$id] = '<h3 class="product-card-product-ingredients-description-header">'.$item["PROPS"]["ACT_STR"]["NAME"].'</h3><div class="product-card-product-ingredients-description-text">';
		$ingr = explode(",", $item["PROPS"]["ACT_STR"]["TXT"]);
		foreach($ingr as $ing) $ingrs[$id] .= '<h5 class="product-card-product-ingredients">'.$ing.'</h5>';
		$ingrs[$id] .= '</div>';
	}
	$ci++;
}
$slider = '<div class="product-card-img-slider-container">
                <div id="options-per-view" class="glide">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides" id="active_slider">'.$slides[$icnt[0]].'</ul>
                    </div>
                    <div class="glide__arrows" data-glide-el="controls">
                        <a class="glide__arrow glide__arrow--left" data-glide-dir="<">&#8249;</a>
                        <a class="glide__arrow glide__arrow--right" data-glide-dir=">">&#8250;</a>
                    </div>
                </div>
                <script>
                    new Glide(".glide", {
                        type: "slider",
                        startAt: 0,
                        perView: 1
                    }).mount()
                </script>
            </div>';
$section_blk = '<div class="product-card-categories-container id="active_section">'.$sections[$icnt[0]].'</div>';
$prodname_blk = '<div class="product-card-product-name-container">
                    <h1 class="product-card-product-name">'.$arResult["ITEMS"][$icnt[0]]["NAME"].'</h1>
                </div>
                <div class="product-card-product-type-container">
                    <h5 class="product-card-product-type">'.$arResult["ITEMS"][$icnt[0]]["PROPS"]["TYPE"]["TXT"].'</h5>
                </div>
                <div class="product-card-sku-container">
                    <h5 class="product-card-sku" id="active_sku">'.$sku[$icnt[0]].'</h5>
                </div>';
if(count($volumes)>0){
	$vol_dd = '<div class="product-card-dropdown-items collapse">';
	$vol_rb = '<div class="product-card-options-container">';
	for($i=0;$i<count($volumes);$i++) {
		if($i==0) { 
			$vol_cb = '<div class="product-card-dropdown-container"><div class="product-card-dropdown-button-container"><h5 class="product-card-dropdown-button" id="active_volume">'.$volumes[$icnt[0]].'</h5></div>'; 
			$vol_cr = ' current';
		} else $vol_cr = '';
		$vol_dd .= '<h5 class="product-card-dropdown-item" onclick="setActiveProduct('.$icnt[$i].');">'.$volumes[$icnt[$i]].'</h5>';
		$vol_pt = explode(" ", $volumes[$icnt[$i]]);
		$vol_rb .= '<a onclick="setActiveProduct('.$icnt[$i].');" href="javascript:void(0)" id="rb-'.$icnt[$i].'" class="product-card-options-button vol-rb'.$vol_cr.'">'.$vol_pt[0].'</a>';
	}
	$vol_cb .= $vol_dd.'</div></div>';
	$vol_rb .= '<h5 class="product-card-options-dimension">'.$vol_pt[1].'</h5></div>';
} else {$vol_cb = $vol_rb = '';}	
if(count($shades)>0){
	$shade_dd = '<div class="product-card-dropdown-items collapse">';
	$shade_rb = '<div class="product-card-options-container">';
	for($i=0;$i<count($shades);$i++) {
		if($i==0) $shade_cb = '<div class="product-card-dropdown-container">
						<div class="product-card-dropdown-button-container">
                            <h5 class="product-card-dropdown-button decor" style="--decor-color: '.$shades[$icnt[0]]["C"].';" id="active_shade">'.$shades[$icnt[0]]["N"].'</h5>
                        </div>';
		$shade_dd .= '<h5 onclick="setActiveProduct('.$icnt[$i].');" class="product-card-dropdown-item decor" style="--decor-color: '.$shades[$icnt[$i]]["C"].';">'.$shades[$icnt[$i]]["N"].'</h5>';
		$shade_rb .= '<a onclick="setActiveProduct('.$icnt[$i].');" href="javascript:void(0)" class="product-card-options-button decor" style="--decor-color: '.$shades[$icnt[$i]]["C"].';"></a>';
	}
	$shade_cb .= $shade_dd.'</div></div>';
	$shade_rb .= '</div>';
} else {$shade_cb = $shade_rb = '';}
$amount_blk = '<div class="product-card-amount-container">
                        <div class="product-card-dropdown-container">
                            <div class="product-card-dropdown-button-container small">
                                <h5 class="product-card-dropdown-button small" id="active_qty">1 шт</h5>
                            </div>
                            <div class="product-card-dropdown-items small collapse">
                                <h5 class="product-card-dropdown-item" onclick="setActiveQty(1);">1 шт</h5>
                                <h5 class="product-card-dropdown-item" onclick="setActiveQty(2);">2 шт</h5>
                                <h5 class="product-card-dropdown-item" onclick="setActiveQty(3);">3 шт</h5>
                            </div>
                        </div>
                    </div>';
$price_blk = '<div class="product-card-price-container">
                    <h1 class="product-card-price"><span id="active_price">'.$prices[$icnt[0]].'</span> &#x20BD;</h1>
                </div>';
$basket_blk = '<div class="product-card-add-to-cart-btn-container" id="active_cartbtn">
					'.$cartbtns.'
					<form class="add-to-basket-detail" action="/cart/add/">
						<input type="hidden" id="frm_id" name="id" value="'.$icnt[0].'">
						<input type="hidden" id="frm_price" name="price" value="'.$prices[$icnt[0]].'">
                        <input type="hidden" id="frm_priceid" name="priceid" value="'.$priceids[$icnt[0]].'">
                        <input type="hidden" id="frm_qty" name="qty" value="1">
					</form>
				</div>';
$favor_blk = '<div class="product-card-add-to-favorites-container">
                    <a href="javascript:void(0)" class="product-card-add-to-favorite-heart">
                        <img src="'.SITE_TEMPLATE_PATH.'/img/product-card-heart-icon.png" alt="" class="product-card-heart-icon">
                    </a>
                    <h5 class="product-card-add-to-favorite-text">'.GetMessage("CT_SF_ADD_TO_FAV").'</h5>
                </div>';
$store_blk = '<div class="product-card-availiability-header-container">
                    <h5 class="product-card-availiability-header">'.GetMessage("CT_SF_AVAIL_TITLE").'</h5>
                </div>
                <div class="product-card-availiability-dropdown-container">
                    <div class="product-card-dropdown-container" id="active_stores">'.$storeblk.'</div>
                </div>';
$descr_blk = '<div class="product-card-product-description">
                    <h3 class="product-card-product-description-header">'.GetMessage("CT_SF_DESCR_TITLE").'</h3>
                    <div class="product-card-product-description-text">
                        <p>'.$arResult["ITEMS"][$icnt[0]]["DETAIL_TEXT"].'</p>
                    </div>
                </div>';
$notes_blk = '<div class="product-card-notes-container" id="active_notes">'.$notes[$icnt[0]].'</div>';	
$cares_blk = '<div class="product-card-care-container" id="active_cares">'.$cares[$icnt[0]].'</div>';
$materials_blk = '<div class="product-card-materials-container" id="active_materials">'.$materials[$icnt[0]].'</div>';
$materials_seta_denticles_blk = '<div class="product-card-materials_seta_denticles-container" id="active_materials_seta_denticles">'.$materials_seta_denticles[$icnt[0]].'</div>';
$usages_blk = '<div class="product-card-product-usage-description" id="active_usages">'.$usages[$icnt[0]].'</div>';
$ingrs_blk = '<div class="product-card-product-ingredients-description" id="active_ingrs">'.$ingrs[$icnt[0]].'</div>';
$arRs = Array("slides"=>$slides,"sections"=>$sections,"sku"=>$sku,"volumes"=>$volumes,"shades"=>$shades,"prices"=>$prices,"priceids"=>$priceids,"notes"=>$notes,"cares"=>$cares,"materials"=>$materials,"materials_seta_denticles"=>$materials_seta_denticles,"usages"=>$usages,"ingrs"=>$ingrs);		
?>
<script>
	var arRs=<?=CUtil::PhpToJSObject($arRs); ?>;
	function setActiveQty(q) {
		$('#active_qty').html(q+' шт');
		$('#frm_qty').val(q);
	}
	function setActiveProduct(id) {
		$('#frm_id').val(id);
		if(!!arRs['slides'][id]) { 
			$('#active_slider').html(arRs['slides'][id]);
			new Glide(".glide", {
                    type: "slider",
					startAt: 0,
                    perView: 1
                }).mount();
		}			
		if(!!arRs['sections'][id]) $('#active_section').html(arRs['sections'][id]);
		if(!!arRs['sku'][id]) $('#active_sku').html(arRs['sku'][id]);
		if(!!arRs['volumes'][id]) { 
			$('#active_volume').html(arRs['volumes'][id]);
			$('.vol-rb').each(function() {
			  $(this).removeClass('current');
			});
			$('#rb-'+id).addClass('current');
		}
		if(!!arRs['shades'][id]) { 
			$('#active_shade').html(arRs['shades'][id]['N']);
			$('#active_shade').attr('style','--decor-color: '+arRs['shades'][id]['C']);
		}
		$('#active_price').html(arRs['prices'][id]);
		$('#frm_price').val(arRs['prices'][id]);
		$('#frm_priceid').val(arRs['priceids'][id]);
		//$('#active_cartbtn').html(arRs['cartbtn'][id]);
		$('.blk-add-to-card-btn').each(function() {
			  if(!$(this).hasClass('blk-add-to-card-btn-hidden')) $(this).addClass('blk-add-to-card-btn-hidden');
			});
		$('#blk-add-to-card-btn-'+id).removeClass('blk-add-to-card-btn-hidden');	
		//if(!!arRs['stores'][id]) $('#active_stores').html(arRs['stores'][id]);
		$('.product-card-dropdown-store-container').each(function() {
			  if(!$(this).hasClass('blk-add-to-card-btn-hidden')) $(this).addClass('blk-add-to-card-btn-hidden');
			});
		$('#product-card-dropdown-store-container-'+id).removeClass('blk-add-to-card-btn-hidden');
		if(!!arRs['notes'][id]) $('#active_notes').html(arRs['notes'][id]);
        if(!!arRs['cares'][id]) $('#active_cares').html(arRs['cares'][id]);
        if(!!arRs['materials'][id]) $('#active_materials').html(arRs['materials'][id]);
        if(!!arRs['materials_seta_denticles'][id]) $('#active_materials_seta_denticles').html(arRs['materials_seta_denticles'][id]);
		if(!!arRs['usages'][id]) $('#active_usages').html(arRs['usages'][id]);
		if(!!arRs['ingrs'][id]) $('#active_ingrs').html(arRs['ingrs'][id]);
	}
</script>
        <div class="product-card-page-container">
            <?=$slider;?>
            <div class="product-card-description-container">
                <?=$brand_name;?>
                <?=$section_blk;?>
				<div id="basket_result"></div>
				<?=$prodname_blk;?>
                <div class="product-card-volume-and-amount-container">
                    <?if($arResult["CARD"]==3) { echo $shade_cb; } else echo $vol_cb;?>
                    <?=$amount_blk;?>
                </div>
                <?if($arResult["CARD"]==3) { echo $shade_rb; } else echo $vol_rb;?>
                <?=$price_blk;?>
                <?=$basket_blk;?>
                <?=$favor_blk;?>
                <?=$store_blk;?>
                <?=$descr_blk;?>
                <?=$materials_blk;?>
                <?=$materials_seta_denticles_blk;?>
                <?if($arResult["CARD"]==1) echo $notes_blk;?>
				<?if($arResult["CARD"]==2) echo $cares_blk.$usages_blk.$ingrs_blk;?>
				<?=$brand_descr;?>
            </div>
        </div>
<?/**/?>
