<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle("Сайт по умолчанию");
?>



<div class="main-sliders-container">
	<?$APPLICATION->IncludeComponent(
		"aspro:com.banners.optimus",
		"top_slider_banners",
		Array(
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"IBLOCK_ID" => "16",
			"IBLOCK_TYPE" => "slider_1",
			"NEWS_COUNT" => "20",
			"PROPERTY_CODE" => array("","URL_STRING",""),
			"SET_BANNER_TYPE_FROM_THEME" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"TYPE_BANNERS_IBLOCK_ID" => "16"
		)
	);?>

	<?$APPLICATION->IncludeComponent(
		"aspro:com.banners.optimus",
		"top_slider_banners",
		Array(
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"IBLOCK_ID" => "17",
			"IBLOCK_TYPE" => "slider_2",
			"NEWS_COUNT" => "20",
			"PROPERTY_CODE" => array("","URL_STRING",""),
			"SET_BANNER_TYPE_FROM_THEME" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"TYPE_BANNERS_IBLOCK_ID" => "17"
		)
	);?>

	<?$APPLICATION->IncludeComponent(
		"aspro:com.banners.optimus",
		"top_slider_banners",
		Array(
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"IBLOCK_ID" => "18",
			"IBLOCK_TYPE" => "slider_3",
			"NEWS_COUNT" => "20",
			"PROPERTY_CODE" => array("","URL_STRING",""),
			"SET_BANNER_TYPE_FROM_THEME" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"TYPE_BANNERS_IBLOCK_ID" => "18"
		)
	);?>

	<?$APPLICATION->IncludeComponent(
		"aspro:com.banners.optimus",
		"top_slider_banners",
		Array(
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"IBLOCK_ID" => "19",
			"IBLOCK_TYPE" => "slider_4",
			"NEWS_COUNT" => "20",
			"PROPERTY_CODE" => array("","URL_STRING",""),
			"SET_BANNER_TYPE_FROM_THEME" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"TYPE_BANNERS_IBLOCK_ID" => "19"
		)
	);?>
</div>

<div class="labaratory-container">
    <div class="labaratory-guide-container">
        <h5>Лабаратория: Scent Guide</h5>
        <div class="labaratory-bg labaratory-scent-guide-bg"></div>
    </div>
    <div class="labaratory-guide-container">
        <h5>Лабаратория: Skincare Guide</h5>
        <div class="labaratory-bg labaratory-skincare-guide-bg"></div>
    </div>
    <div class="labaratory-guide-container">
        <h5>Лабаратория: Haircare Guide</h5>
        <div class="labaratory-bg labaratory-haircare-guide-bg"></div>
    </div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	".default", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "83",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "aspro_optimus_content",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add",
	"",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALLOW_DELETE" => "Y",
		"ALLOW_EDIT" => "Y",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "PROPERTY_ID",
		"ELEMENT_ASSOC_PROPERTY" => "",
		"GROUPS" => array("1"),
		"IBLOCK_ID" => "20",
		"IBLOCK_TYPE" => "aspro_optimus_content",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"NAV_ON_PAGE" => "2",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(),
		"PROPERTY_CODES_REQUIRED" => array(),
		"RESIZE_IMAGES" => "Y",
		"SEF_FOLDER" => "/include/menu/",
		"SEF_MODE" => "Y",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N"
	)
);?>