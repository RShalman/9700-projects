<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


    <div id="options-per-view" class="glide">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides main-page-carousel" id="active_slider">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
                    <a href="<?=$arItem["DISPLAY_PROPERTIES"]["BANNER_LINK"]["VALUE"];?>">
                        <li class="glide__slide slider-container" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <div class="slider">
                                <div class="slider-content">
                                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["P1"]["FILE_VALUE"]["SRC"];?>">
                                </div>
                            </div>
                            <div class="slider">
                                <div class="slider-content">
                                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["P2"]["FILE_VALUE"]["SRC"];?>">
                                </div>
                            </div>
                            <div class="slider">
                                <div class="slider-content">
                                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["P3"]["FILE_VALUE"]["SRC"];?>">
                                </div>
                            </div>
                            <div class="slider">
                                <div class="slider-content">
                                    <img src="<?=$arItem["DISPLAY_PROPERTIES"]["P4"]["FILE_VALUE"]["SRC"];?>">
                                    <!-- <div class="slider-text-content">

                                        <h1 class="slider-description-header">
                                            <?=$arItem["NAME"];?>
                                        </h1>
                                        <h5 class="slider-description-text">
                                            <?=$arItem["PREVIEW_TEXT"];?>
                                        </h5>
                                    </div> -->
                                </div>
                            </div>
                        </li>
                    </a>
                        <?endforeach;?>
            </ul>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        new Glide(".glide", {
            type: "carousel",
            startAt: 0,
            perView: 1,
            autoplay: 5000,
            hoverpause: true,
            keyboard: true,
            animationDuration: 1000
        }).mount()
    });
    </script>